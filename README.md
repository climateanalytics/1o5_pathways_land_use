# IKEA 1o5 Pathways - Land use sectors

The repository stores scripts, analyses, literature, and datasets needed to estimate emissions/removals from land-use sectors

# Installation
```
$ conda env create -n land_use --file environment.yml
$ conda activate land_use
```

then configure datatoolbox, i.e. run the following and provide initials and full path to datashelf

```python
import datatoolbox as dt
dt.admin.change_personal_config()
```
