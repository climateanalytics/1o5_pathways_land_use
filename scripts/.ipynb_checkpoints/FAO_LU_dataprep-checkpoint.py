#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 12:24:45 2020

@author: clairefyson
"""

import pandas as pd
import datatoolbox as dt
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


# In[40]:


# pd.set_option('display.max_rows', None)



land_ids = ['Area|Land|Total__Historic__FAO_2020','Area|Land|Agriculture|Cropland__Historic__FAO_2020',
                 'Area|Land|Agriculture|Meadows_and_pastures__Historic__FAO_2020', 'Area|Land|Other__Historic__FAO_2020',
                 'Area|Land|Forest|Other_naturally_regenerated_forest__Historic__FAO_2020', 'Area|Land|Forest|Planted_forest__Historic__FAO_2020',
                 'Area|Land|Forest|Primary_forest__Historic__FAO_2020', 'Area|Land|Country_area__Historic__FAO_2020']
# what about e.g. temporary pasture / temporary crops / temporary fallow?
# 
ag_ids = ['Area|Land|Agriculture|Cropland__Historic__FAO_2020','Area|Land|Agriculture|Meadows_and_pastures__Historic__FAO_2020']
forest_ids = ['Area|Land|Forest|Planted_forest__Historic__FAO_2020','Area|Land|Forest|Other_naturally_regenerated_forest__Historic__FAO_2020',
             'Area|Land|Forest|Primary_forest__Historic__FAO_2020']


# In[3]:


for_sum = sum([dt.getTable(id) for id in forest_ids])
ag_sum = sum([dt.getTable(id) for id in ag_ids])
other = dt.getTable('Area|Land|Other__Historic__FAO_2020')
land_tab = dt.getTable('Area|Land|Total__Historic__FAO_2020')

area_dataset = dt.getTables(land_ids)

# In[7]:


def aggregateToRegion(tableSet, mapping):
    missingCountryDf = pd.DataFrame(columns=mapping.listAll())
    for tableKey in tableSet.keys():
        for region in mapping.listAll():
#                print(region)
            missingCountries = set(mapping.membersOf(region)) - set(tableSet[tableKey].index)
#                print('missing countries: {}'.format(missingCountries))
            missingCountryDf.loc[tableSet[tableKey].ID, region] = list(missingCountries)
            availableCountries = set(mapping.membersOf(region)).intersection(tableSet[tableKey].index)
            if len(availableCountries) >0:
                tableSet[tableKey].loc[region,:] = tableSet[tableKey].loc[availableCountries,:].sum(axis=0)
    return tableSet, missingCountryDf


# In[14]

area_dataset_reg, area_missing_df = aggregateToRegion(area_dataset, dt.mapp.regions.IPCC_15)

area_table = area_dataset_reg.to_LongTable()

years_of_interest = np.arange(2010, 2018)

area_shares_tab = pd.DataFrame()

for idx in area_table.index:
    var = area_table.loc[idx, 'variable']
    country = area_table.loc[idx, 'region']
    tempdf = ()
    if country in dt.mapp.regions.IPCC_15.listAll():
        continue
    try:
        region_value = area_table.loc[(area_table['variable'] == var) & (area_table['region'] == dt.mapp.regions.IPCC_15.regionOf(country)[0]), years_of_interest]
        new_row = area_table.loc[idx, years_of_interest] / region_value * 100
        temp_df = pd.DataFrame(new_row)
        temp_df['region'] = country
        temp_df['variable'] = 'Share_' + var
        area_shares_tab = pd.concat([area_shares_tab, temp_df]) 
    except IndexError:
        continue
    
area_shares_tab['Unit'] = '%'


### doesn't work...
#---
#area_share_tabs = dict()

#for key in area_dataset_reg.keys():
#    area_share_tabs[area_dataset_reg[key].meta['variable']] = area_dataset_reg[key].copy()
#    for idx in area_dataset_reg[key].index:
#        if idx in ['IPCC_OECD90+EU', 'IPCC_REF', 'IPCC_ASIA', 'IPCC_MAF', 'IPCC_LAM']:
#            continue
#        area = area_dataset_reg[key].loc[idx]
#        region_tot = area_dataset_reg[key].loc[dt.mapp.regions.IPCC_15.regionOf(idx)]
#        #area_dataset_reg[key].loc[idx, 'region_name'] = dt.mapp.regions.IPCC_15.regionOf(idx)
#        area_share_tabs[area_dataset_reg[key].meta['variable']].loc[idx] = area / region_tot
#----
        

### EMISSIONS
FAO_tableIDs = dt.find(variable = 'Emissions', source='FAO_2020')
FAO_tableIDs_N2O = dt.find(variable = 'N2O', source='FAO_2020')
FAO_tableIDs_CH4 = dt.find(variable = 'CH4', source='FAO_2020')
FAO_tableIDs_CO2 = dt.find(variable = 'CO2', source='FAO_2020')

FAO_tableIDs_gases = FAO_tableIDs_N2O.append(FAO_tableIDs_CH4)
FAO_tableIDs_gases = FAO_tableIDs_gases.append(FAO_tableIDs_CO2)

FAO_ag_IDs = dt.find(variable = 'Emissions', category = "Agriculture", source = 'FAO_2020')
FAO_lu_IDs = dt.find(variable = 'Emissions', category = "FOLU", source = 'FAO_2020')
FAO_ag_IDs = FAO_ag_IDs.append(FAO_lu_IDs) # list of table IDs for ag and LU

# Convert all tables to CO2eq (AR4)
tablesGases = dt.getTables(FAO_tableIDs.index)
for key in tablesGases.keys():
    if 'Gg' in tablesGases[key].meta['unit']:
        tablesGases[key] = tablesGases[key].convert(tablesGases[key].meta['unit'].replace('Gg','Mt'))
    if tablesGases[key].meta['unit'] != 'Mt CO2eq':
        tablesGases[key] = tablesGases[key].convert('Mt CO2eq', 'GWPAR4')
        
tableset_aglu = dt.getTables(FAO_ag_IDs.index)
for key in tableset_aglu.keys():
    if 'Gg' in tableset_aglu[key].meta['unit']:
        tableset_aglu[key] = tableset_aglu[key].convert(tableset_aglu[key].meta['unit'].replace('Gg','Mt'))
    if tableset_aglu[key].meta['unit'] != 'Mt CO2eq':
        tableset_aglu[key] = tableset_aglu[key].convert('Mt CO2eq', 'GWPAR4')

tableset_aglu, em_missing_df = aggregateToRegion(tableset_aglu, dt.mapp.regions.IPCC_15)

tables_aglu = tableset_aglu.to_LongTable()

crop_variables = ['Emissions|GHG|Agriculture|Agricultural_soils', 
                'Emissions|GHG|Agriculture|Burning|Crop_residues',
                'Emissions|GHG|Agriculture|Crop_residues',
                'Emissions|GHG|Agriculture|Cultivation_organic_soils',
                'Emissions|GHG|Agriculture|Rice_cultivation',
                'Emissions|GHG|Agriculture|Synthetic_fertilizers',
                'Emissions|GHG|Agriculture|Manure|Applied',
                'Emissions|CH4|Agriculture|Burning|Crop_residues',
                'Emissions|CH4|Agriculture|Rice_cultivation',
                'Emissions|N2O|Agriculture|Burning|Crop_residues',
                'Emissions|N2O|Agriculture|Crop_residues',
                'Emissions|N2O|Agriculture|Cultivation_organic_soils',
                'Emissions|N2O|Agriculture|Manure|Applied_to_soils',
                'Emissions|N2O|Agriculture|Synthetic_fertilizers']


crop_var_names = ['Burning|Crop_residues',
                  'Agriculture|Crop_residues',
                  'Cultivation_organic_soils',
                  'Rice_cultivation',
                  'Synthetic_fertilizers',
                  'Manure|Applied']
# Notes: 
## cultivation of organic soils includes pastureland - need to use FAO data to distinguish cropland from pastureland soils
## note manure applied on soils could be put as a pasture variable
## 'agricultural soils is a compilation of all soil-related items (cropland and grassland)

pasture_variables = ['Emissions|GHG|Agriculture|Enteric_Fermentation',
                     'Emissions|GHG|Agriculture|Manure|management',
                     'Emissions|GHG|Agriculture|Manure|Left_on_pasture',
                     'Emissions|GHG|Agriculture|Burning|Savanna',
                     'Emissions|CH4|Agriculture|Burning|Savanna',
                     'Emissions|CH4|Agriculture|Enteric_Fermentation',
                     'Emissions|CH4|Agriculture|Manure|Management',
                     'Emissions|N2O|Agriculture|Burning|Savanna',
                     'Emissions|N2O|Agriculture|Manure|Management',
                     'Emissions|N2O|Agriculture|Manure|Left_on_pasture']

pasture_var_names = ['Enteric_Fermentation', 'Manure|management',
                     'Manure|Left_on_pasture', 'Burning|Savanna' ]

ag_total_variables = ['Emissions|CH4|Agriculture', 'Emissions|N2O|Agriculture',
                      'Emissions|F-gases|Agriculture']

#F-gases are measured in Gg N2O...

ag_lu_variables = ['Emissions|CH4|Agriculture|Land_use',
                   'Emissions|CO2|Agriculture|Land_use']

lu_variables = ['Emissions|CH4|FOLU','Emissions|N2O|FOLU', 'Emissions|CO2|FOLU',
                'Emissions|CO2|FOLU|Burning_biomass', 'Emissions|CO2|FOLU|Cropland',
                'Emissions|CO2|FOLU|Forest', 'Emissions|CO2|FOLU|Grassland',
                'Emissions|GHG|FOLU|Burning_biomas', 'Emissions|GHG|FOLU|Cropland',
                'Emissions|GHG|FOLU|Forest', 'Emissions|GHG|FOLU|Grassland',
                'Emissions|GHG|FOLU']

#burning_bio_variable = ['Emissions|CO2|FOLU|Burning_biomass']

# Drop GHG items
#... to do

#tables_aglu_noghg = tables_aglu.loc[tables_aglu['unit'] != 'Mt CO2eq']
#tables_aglu_noghg = tables_aglu_noghg.loc[tables_aglu['unit'] != 'Gg N2O']
#tables_aglu_noghg = tables_aglu_noghg.loc[tables_aglu['unit'] != 'Gg CO2eq']

tables_aglu_noghg = tables_aglu[~tables_aglu['variable'].str.contains('GHG')]

CO2eq_df = pd.DataFrame()
for var in crop_var_names:
    vfilt = tables_aglu_noghg[tables_aglu_noghg['variable'].str.endswith(var)]
   # if vfilt.empty:
   #     continue
    for reg in tables_aglu_noghg.region.unique():
        rfilt = vfilt[(vfilt['region'] == reg)]
    #    if rfilt.empty:
     #       continue
        totals = rfilt.loc[:,years_of_interest].sum(axis = 0)
        totals_df = pd.DataFrame(totals, columns = ['value'])
        totals_df['region'] = reg
        totals_df['variable'] = var
        totals_df['category'] = 'cropland'
        CO2eq_df = pd.concat([CO2eq_df, totals_df])


for var in pasture_var_names:
    vfilt = tables_aglu_noghg[tables_aglu_noghg['variable'].str.endswith(var)]
    for reg in tables_aglu_noghg.region.unique():
        rfilt = vfilt[(vfilt['region'] == reg)]
        totals = rfilt.loc[:,years_of_interest].sum(axis = 0)
        totals_df = pd.DataFrame(totals, columns = ['value'])
        totals_df['region'] = reg
        totals_df['variable'] = var
        totals_df['category'] = 'pastureland'
        CO2eq_df = pd.concat([CO2eq_df, totals_df])
        
CO2eq_df['unit'] = 'Mt CO2eq'
CO2eq_df['GWP'] = 'AR4'
CO2eq_df = CO2eq_df.reset_index()
CO2eq_df = CO2eq_df.rename(columns = {'index': 'year'})

## TESTING: Filtering table to check values against FAO GHG values
CO2eq_df[(CO2eq_df['variable'].str.endswith('Burning|Crop_residues')) & (CO2eq_df['region'] == 'IPCC_LAM') & (CO2eq_df.index == 2017)]

tables_aglu[tables_aglu['variable'].str.endswith('Burning|Crop_residues') & (tables_aglu['region'] == 'IPCC_LAM')]      

df = tables_aglu
df[(df['year'] == 2017) & (df['region'] == 'IPCC_LAM')] 



# Aggregating to cropland and pastureland 
lst = []
cols = CO2eq_df.columns
for yr in years_of_interest:
    for cat in ['cropland', 'pastureland']:
        for reg in CO2eq_df.region.unique():
            rfilt = (CO2eq_df['region'] == reg)
            catfilt = (CO2eq_df['category'] == cat)
            total = CO2eq_df.loc[catfilt & rfilt & (CO2eq_df['year'] == yr), 'value'].sum()
            lst.append([yr, total, reg, cat + '_ag', cat + '_ag', 'Mt CO2eq', 'AR4'])
crop_pasture_df = pd.DataFrame(lst, columns = cols)

lst = []
for yr in years_of_interest:
    for reg in crop_pasture_df.region.unique():
        rfilt = (crop_pasture_df['region'] == reg)
        total_ag = crop_pasture_df.loc[(crop_pasture_df['category'].isin(['pastureland_ag', 'cropland_ag'])) & (crop_pasture_df['year'] == yr) & rfilt, 'value'].sum()
        lst.append([yr, total_ag, reg, 'agriculture_total', 'agriculture_total', 'Mt CO2eq', 'AR4'])
        #total_df = total_df.reset_index()
        #total_df = total_df.rename(columns = {'index': 'year'})
ag_total_df = pd.DataFrame(lst, columns = cols)

#CO2eq_df = pd.concat([CO2eq_df, crop_pasture_df])

#x = []
#y = []
#for reg in dt.mapp.regions.IPCC_15.listAll():
 #   val = CO2eq_df.loc[(CO2eq_df['year'] == 2017) & (CO2eq_df['region'] == reg) & (CO2eq_df['variable'] == 'Enteric_Fermentation'), 'value'].values
  #  val2 = tables_aglu.loc[(tables_aglu['region'] == reg) & (tables_aglu['variable'] == 'Emissions|GHG|Agriculture|Enteric_Fermentation'), 2017].values
   # x.append(val)
   # y.append(val2)
   # print(val/val2)
   # sns.regplot(x = val, y = val2)
   
## LU emissions

table_LU = tables_aglu[tables_aglu['variable'].str.contains('FOLU')]

lu_variables = ['Emissions|CH4|FOLU','Emissions|N2O|FOLU', 'Emissions|CO2|FOLU',
                'Emissions|CO2|FOLU|Burning_biomass', 'Emissions|CO2|FOLU|Cropland',
                'Emissions|CO2|FOLU|Forest', 'Emissions|CO2|FOLU|Grassland',
                'Emissions|GHG|FOLU|Burning_biomass', 'Emissions|GHG|FOLU|Cropland',
                'Emissions|GHG|FOLU|Forest', 'Emissions|GHG|FOLU|Grassland',
                'Emissions|GHG|FOLU']

lu_variables_touse = ['Emissions|GHG|FOLU|Burning_biomass', 'Emissions|GHG|FOLU|Cropland',
                'Emissions|GHG|FOLU|Forest', 'Emissions|GHG|FOLU|Grassland',
                'Emissions|GHG|FOLU']

burning_biomass_var = ['Emissions|CH4|FOLU', 'Emissions|N2O|FOLU', 
                           'Emissions|CO2|FOLU|Burning_biomass']

soilLUC_var = ['Emissions|CO2|FOLU|Grassland', 'Emissions|GHG|FOLU|Cropland']
    
def aggregate_variables(table, variables_list, years, variable_name, category_name):
    df = pd.DataFrame()
    for reg in table.region.unique():
        rfilt = (table['region'] == reg)
        filt = table.loc[rfilt & (table_LU['variable'].isin(variables_list)), years].sum()
        total_df = pd.DataFrame(filt, columns = ['value'])
        total_df['region'] = reg
        total_df['variable'] = variable_name
        total_df['category'] = category_name
        total_df = total_df.reset_index()
        total_df = total_df.rename(columns = {'index': 'year'})
        total_df['unit'] = 'Mt CO2eq'
        total_df['GWP'] = 'AR4'
        df = pd.concat([df, total_df])
    return df

land_df = aggregate_variables(table_LU, ['Emissions|CH4|FOLU', 'Emissions|N2O|FOLU', 'Emissions|CO2|FOLU'], years_of_interest, 'FOLU_total', 'FOLU_total')
        
burning_bio_df = aggregate_variables(table_LU, burning_biomass_var, years_of_interest, 'burning_bio_total', 'burning_biomass_folu')

# Make long table with only LU variables of interest

lu_df = pd.DataFrame()           
for yr in years_of_interest:
    for idx in table_LU.index:
        if table_LU.loc[idx, 'variable'] in lu_variables_touse:
            val = table_LU.loc[idx, yr]
            row = [yr, val, table_LU.loc[idx, 'region'], table_LU.loc[idx, 'variable'], table_LU.loc[idx, 'variable'][14:]]
            tempdf = pd.DataFrame([row], columns = cols[0:5])
            lu_df = pd.concat([lu_df, tempdf])
lu_df['Unit'] = 'Mt CO2eq'
lu_df['GWP'] = 'AR4'

file_path = '/Users/clairefyson/Box/Climate Policy Team/02 - Projects/IKEA NDC 1.5° Pathways 20-22/2- Work Packages/WP3 -  LULUCF Pathways/Workflow/'
area_shares_tab.to_excel(file_path + 'area_shares_hist.xlsx')
area_table.to_excel(file_path + 'fao_areas_hist.xlsx')
crop_pasture_df.to_excel(file_path + 'crop_pasture_hist_ems.xlsx')
lu_df.to_excel(file_path + 'lu_hist_ems.xlsx')

## plot emissions intensity vs. land area?