#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 14:24:04 2020

@author: clairefyson
"""
# Code to extract reforestation areas / removals potentials from 
# Griscom et al and Brancalion et al 

import pandas as pd
import numpy as np
import datatoolbox as dt
import seaborn as sns
import matplotlib.pyplot as plt

# Reforestation potential

## Griscom et al sequestration per ha per yr (SI page p7)

CperHa = dict()
CperHa['temperate'] = 2.82
CperHa['tropical'] = 4.71
CperHa['global'] = 4.14
CperHa['unit'] = 'tC/ha/yr'
CperHa['dooley_reforestation_trop'] = 3.1

# Note that these seem quite optimistic

file_path = '/Users/clairefyson/Box/Climate Policy Team/02 - Projects/IKEA NDC 1.5° Pathways 20-22/2- Work Packages/WP3 -  LULUCF Pathways/Workflow/Data/'
griscom = pd.read_excel(file_path + 'Griscom_potentials.xlsx', header = 1)

for cntry in griscom.Country.unique():
    griscom.loc[griscom['Country'] == cntry, 'country_code'] = dt.getCountryISO(cntry)
    
# drop duplicates
griscom= griscom.drop_duplicates(subset = 'country_code', keep = 'last')

griscom = griscom.set_index('country_code')

griscom = griscom.rename(columns = {'Reforestation': 'Reforestation_removals_MtCO2/yr'})

# estimate land area reforested (using global sequestration per ha)
temperate_countries = ['ARG', 'AUT', 'BEL', 'CAN', 'CHL', 'CHN',
                       'DNK', 'FIN', 'FRA', 'DEU', 'GRC', 
                       'HUN', 'IRN', 'IRL', 'ISR', 'ITA', 'JPN',
                       'JOR', 'KOR', 'LUX', 'MEX', 'NLD', 'NZL',
                       'NOR', 'PAK', 'POL', 'PRT', 'RUS', 'ESP',
                       'SWE', 'CHE', 'TUN', 'TUR', 'GBR', 'USA',
                       'URY']
# difficult to get a definition of which countries are in the temperate zone'
for cntry in griscom.index:
    if cntry in temperate_countries:
        griscom.loc[cntry, 'ref_area_Mha'] = griscom.loc[cntry, 'Reforestation_removals_MtCO2/yr'] / (CperHa['global'] * 3.66667)
        # conservative area estimate from using a high value of C/ha
    else:
        griscom.loc[cntry, 'ref_area_Mha'] = griscom.loc[cntry, 'Reforestation_removals_MtCO2/yr'] / (CperHa['tropical'] * 3.66667)

griscom.loc['World', :] = griscom[:].sum()

griscom['unit_removals'] = 'Mt CO2/yr'
griscom['unit_area'] = 'Mha'

# find reforestation removals share of total
griscom['reforestation_removals_share'] = griscom['Reforestation_removals_MtCO2/yr'] / griscom.loc['World', 'Reforestation_removals_MtCO2/yr']



## Brancalion data
brancalion = pd.read_excel(file_path + 'Brancalion_restoration_potential.xlsx', header = 1)

brancalion = brancalion.rename(columns = {'Unnamed: 3': 'Standard_dev'})
brancalion = brancalion[(brancalion.index != 0) & (brancalion.Country != 'Country')]
brancalion = brancalion[brancalion['Standard_dev'] != 'standard deviation']


#brancalion['country_code'] = []
for cntry in brancalion.Country.unique():
    brancalion.loc[brancalion['Country'] == cntry, 'country_code'] = dt.getCountryISO(cntry)

# Find country code for 'Dem. Rep.' (DRC)
brancalion.loc[brancalion['Country'] == 'Dem. Rep.', 'country_code'] = 'COD'  

# drop duplicates
brancalion= brancalion.drop_duplicates(subset = 'country_code', keep = 'last')

brancalion = brancalion.set_index('country_code')
brancalion.columns = ['Country', 'Continent', 'Restoration_Score', 'Standard_dev',
       'Restorable_Area_km2', 'Restoration_Score_>0.6_km2',
       'Bonn_Challenge_pledge_km2', 'Unnamed:7']
brancalion = brancalion.drop('Unnamed:7', axis = 1)

brancalion.loc['World', 'Restoration_Score_>0.6_km2'] = brancalion['Restoration_Score_>0.6_km2'].sum()
brancalion.loc['World', 'Bonn_Challenge_pledge_km2'] = brancalion['Bonn_Challenge_pledge_km2'].sum()
brancalion.loc['World', 'Restorable_Area_km2'] = brancalion['Restorable_Area_km2'].sum()

brancalion['Share_restorable_>0.6'] = brancalion['Restoration_Score_>0.6_km2'] / brancalion.loc['World','Restoration_Score_>0.6_km2']

# Combine tables
temp1 = brancalion[['Restorable_Area_km2', 'Restoration_Score_>0.6_km2']]
temp2 = griscom[['Reforestation_removals_MtCO2/yr', 'ref_area_Mha']]
reforest_tab = temp1.merge(temp2, how = 'outer', left_index = True, right_index = True)

reforest_tab['b_restorable_highscore_Mha'] = reforest_tab['Restoration_Score_>0.6_km2']*100/1E6
reforest_tab['b_restorable_area_Mha'] = reforest_tab['Restorable_Area_km2']*100/1E6
reforest_tab['brancalion_highscore_share_%'] = reforest_tab['b_restorable_highscore_Mha']/reforest_tab.loc['World','b_restorable_highscore_Mha'] * 100
reforest_tab['griscom_share_%'] = reforest_tab['ref_area_Mha']/reforest_tab.loc['World','ref_area_Mha'] * 100
#reforest_tab['griscom_removals_share_%'] = reforest_tab['Reforestation_removals_MtCO2/yr']/reforest_tab.loc['Total','Reforestation_removals_MtCO2/yr'] * 100

# Find a 'combined' reforestation area -> brancalion et al high score values for tropical countries; 
# griscom for other countries

branc_trop = set(brancalion.index) - set(temperate_countries) - {'World'}
griscom_rest = set(reforest_tab.index) - branc_trop - {'World'} - set(dt.mapping.regions.R5.listAll())

reforest_tab.loc[branc_trop, 'combined'] = reforest_tab.loc[branc_trop, 'b_restorable_highscore_Mha']
reforest_tab.loc[griscom_rest, 'combined'] = reforest_tab.loc[griscom_rest, 'ref_area_Mha']

# Add regions
def aggregateToRegionRef(table, mapping):
    missingCountryDf = pd.DataFrame(columns=mapping.listAll())
    for region in mapping.listAll():
#               print(region)
        missingCountries = set(mapping.membersOf(region)) - set(table.index)
#        print('missing countries: {}'.format(missingCountries))
        missingCountryDf.loc['reforest_table', region] = list(missingCountries)
        availableCountries = set(mapping.membersOf(region)).intersection(table.index)
        table.loc[region,:] = table.loc[availableCountries,:].sum(axis=0)
        table.loc[availableCountries, 'world_region'] = region
    return table, missingCountryDf

reforest_tab2, missing_countries = aggregateToRegionRef(reforest_tab, dt.mapp.regions.R5)

reforest_tab2.loc[temperate_countries, 'zone'] = 'temp_bor'
tropical = set(griscom.index) - set(temperate_countries) - {'World'}
reforest_tab2.loc[tropical, 'zone'] = 'trop_sub'
# not that some 'tropical' countries may be in the temperate zone'

# Find reforestation potential for each region
#for reg in dt.mapping.regions.R5.listAll():
#    tempList = dt.mapping.regions.R5.membersOf(reg).intersection(temperate_countries)
#    tropList = set(dt.mapping.regions.R5.membersOf(reg)) - set(temperate_countries)
#    reforest_tab2.loc[reg + '_temp',:] = reforest_tab2.loc[tempList,:].sum()
#    reforest_tab2.loc[reg + '_trop',:] = reforest_tab2.loc[tropList,:].sum()



#for idx in reforest_tab2.index:
#    if idx in temperate_countries:
#        reforest_tab2.loc[idx, 'combined'] = reforest.loc[idx, 'ref_area_Mha']
#    if idx in dt.mapping.regions.R5.listAll():
#        reforest_tab2.loc[idx, 'combined'] = reforest.loc[idx + '_temp', 'ref_area_Mha'] + reforest.loc[idx + '_trop', 'ref_area_Mha']
#    else:
#        reforest_tab2.loc[idx, 'combined'] = reforest.loc[idx, 'b_restorable_highscore_Mha']

reforest_tab2.to_excel(file_path + 'reforestation_areas.xlsx')
