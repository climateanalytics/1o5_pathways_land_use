# -*- coding: utf-8 -*-
"""
Owner / editor: Firza Riany
Email: firza.riany@climateanalytics.org
Last update: 15.11.2021, 19:12 CEST
Library: functions for emissions trajectory and land use pattern
"""

# libraries
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import rasterio as rio


# preparing dataset for timeseries plot

def timeseries_df(paste_regions, paste_vars, df_input_raw, variable_colnames=False):
    # subset dataset based on the variable and region of interest
    # paste_regions and paste_vars should be given in "[]"
    region_to_use = paste_regions
    var_to_use = paste_vars

    subset_df = df_input_raw.loc[(df_input_raw.Variable.isin(var_to_use)) & 
                             (df_input_raw.Region.isin(region_to_use))]
    
    # tidying the subset df
    years = []
    for x in df_input_raw.iloc[:, 5:25]:
        years.append(x)
        
    if variable_colnames==False:    
        timeseries_df = pd.melt(subset_df,
                                id_vars=["Region", "Model", "Scenario", "Variable",
                                         "Unit"],
                                value_vars=years,
                                var_name= "years", value_name="values")
        return timeseries_df
    
    else:
        timeseries_long = pd.melt(subset_df,
                                  id_vars=["Region", "Model", "Scenario", "Variable",
                                         "Unit"],
                                  value_vars=years,
                                  var_name= "years", value_name="values")
        
        list_names = []
        for names in timeseries_long.Variable:
            list_names.append(names.split("|")[-1])
            
        timeseries_long.Variable = list_names
    
        return timeseries_long

"""
timeseries_df function is used for tidying a wide df to a long df so that 
the df is ready for timeseries plotting. 
it first subsets the df based on the variable(s) and region(s) of interest. 
then it melts the subset df using pd.melt to transform the df from wide to 
long df, which is practical for plotting a timeseries. it returns a long tidy 
df, ready for timeseries plotting.

Parameters:

- paste_regions: a list of str containing the names of the countries of interest 
i.e. ["BRA"] or ["BRA", "IDN", "PNG"]
- paste_vars: a list of str containing the names of the variables of interest 
i.e. ["Emissions|CO2|AFOLU|Land"] or 
["Emissions|CO2|AFOLU|Land", "Carbon Sequestration|Land Use"]
- df_input_raw: a DataFrame where the data is sourced from

Returns:

- a long DataFrame with the renamed axis labels
"""

# function to separate emissions from net emissions

def pure_emis(df_input_long, vars_label, give_seq_vars, give_netemis_vars):
    ts_df_seq = df_input_long.loc[df_input_long[vars_label] == give_seq_vars] # get the sequestration data
    ts_df_netemis = df_input_long.loc[df_input_long[vars_label] == give_netemis_vars] # get the net emission data
    
    ts_vals_ems = [] # a new list to store pure emissions
    
    for x, y in zip(ts_df_netemis["values"], ts_df_seq["values"]):
        ts_vals_ems.append(x - (-(y)))
        
    ts_df_emis = pd.DataFrame({"Region" : ts_df_netemis.Region, 
                               "Model": ts_df_netemis.Model, 
                               "Scenario" : ts_df_netemis.Scenario,
                               "Variable" : "Pure_Emissions|CO2|AFOLU|Land", 
                               "Unit" : ts_df_netemis.Unit,
                               "years" : ts_df_netemis.years, 
                               "values" : ts_vals_ems})
    
    new_df = pd.concat([df_input_long, ts_df_emis]).sort_values(by=["years"])
        
    return new_df

"""
pure_emis function is used to separate pure emission from net emission. 
The function takes the values of net emission and of sequestration across 
the years then subtract net emission with sequestration.  

Parameters:  
- df_input_long: a long DataFrame containing the data of the variables of interest  
- vars_label: a str type, the label of the variable column in the DataFrame 
i.e. "Variable" or "variable" or "vars"  
- give_seq_vars: a str type, the given name of sequestration variable nested 
within the variable column i.e. "Carbon Sequestration|Land Use"  
- give_netemis_vars: a str type, the given name of net emission variable 
nested within the variable column i.e. "Emissions|CO2|AFOLU|Land"  

Returns:  
- a long DataFrame with additional variable called "Pure_Emissions|CO2|AFOLU|Land" 
containing a separate CO2 emissionsa long DataFrame containing the data of 
the variables of interest from net emissions
"""

# plotting function: timeseries for each land use type

def timeseries_pattern(df_input_long, give_x, give_y, vars_label):
    sns.set_theme(style="dark")
    
    p = sns.relplot(
        data=df_input_long,
        x=give_x, y=give_y, col=vars_label, hue=vars_label,
        kind="line", palette="crest", linewidth=4, zorder=5,
        col_wrap=3, height=2, aspect=1.5, legend=False,
    )

    # Iterate over each subplot to customize further
    for variable, ax in p.axes_dict.items():
        # Plot every year's time series in the background
        sns.lineplot(
            data=df_input_long, x=give_x, y=give_y, units=vars_label,
            estimator=None, color=".7", linewidth=1, ax=ax,
        )
        
    # Reduce the frequency of the x axis ticks
    ax.set_xticks(ax.get_xticks()[::3])
    
    # Tweak the supporting aspects of the plot
    p.set_axis_labels("Years", "million ha")
    p.tight_layout();
    
"""
timeseries_pattern plots the evolution of each land use type across the years. 
It takes the land area of each land use type from the earliest year to 2100 and 
creates one timeseries plot for each land use type.  

Parameters:  
- df_input_long: a long DataFrame containing the data of the variables of interest  
- give_x: a str type, the label of the year variable we want to plot on the 
x-axis i.e. "years"  
- give_y: a str type, the label of the land area variable we want to plot on 
the y-axis i.e. "values". 
- vars_label: a str type, the label of the variable column in the DataFrame 
i.e. "Variable" or "variable" or "vars"  

Returns:  
- Timeseries plots of land area from the earliest year in the DataFrame to 2100
for each land use type
"""

# plotting function: stackplot for each land use type

def stackplot_pattern(df_input_long, year_label, vars_label, values_label):
    dict_values = {}
    years = np.unique(df_input_long[year_label].values)
    
    for temp in df_input_long[vars_label].unique():
        dict_values[temp] = df_input_long.loc[df_input_long[vars_label] == temp][[values_label]].to_numpy()
        
    labs = list(dict_values.keys())
    
    dict_values_2 = {}
    for keys in dict_values:
        dict_values_2[keys] = np.concatenate(dict_values[keys]).ravel()
    
    plt.figure(figsize=[8,5])
    plt.stackplot(years, list(dict_values_2.values()), labels=labs)
    plt.legend(loc='upper center', bbox_to_anchor=(1.1, 0.8), shadow=True, ncol=1)
    plt.xticks(rotation=40);
    
"""
stackplot_pattern stacks the evolution of each land use type across the years.
It takes the land area of each land use type from the earliest year to 2100.  

Parameters:  
- df_input_long: a long DataFrame containing the data of the variables of 
interest  
- year_label: a str type, the label of the year variable we want to plot on 
the x-axis i.e. "years"  
- vars_label: a str type, the label of the variable column in the DataFrame 
i.e. "Variable" or "variable" or "vars"  
- values_label: a str type, the label of the land area variable we want to plot 
on the y-axis i.e. "values"

Returns:  
- A stacked timeseries plot of land area from the earliest year in the 
DataFrame to 2100 for each land use type
"""

# fun for land_type distribution
def dist_reg_2(variable, land_type, date, reg_name):
    dist_df = variable.sel(NGLNDCOV = land_type, time = date)
    
    reg = reg_name
    # g_reg = reg.geometry

    # alternative, pbb faster
    #clipped = dist_df.rio.clip_box(*g_reg.bounds, crs=dist_df.rio.crs)

    clipped_dist = dist_df.rio.clip_box(minx = min(reg.bounds.minx), 
                                        miny = min(reg.bounds.miny),
                                        maxx = max(reg.bounds.maxx), 
                                        maxy = max(reg.bounds.maxy))
    
    return clipped_dist

# function to get the distribution map of a land type

def land_dist(ncdf, land_type, date, reg_name):
    dist_df = ncdf.sel(NGLNDCOV = land_type, time = date)
    
    reg = reg_name
    g_reg = reg.geometry
    
    clipped_dist = dist_df.rio.clip(g_reg, dist_df.rio.crs)
    
    
    return clipped_dist

"""
land_dist acquires a distribution map for a given land use type at the country-
level. It crops a global distribution map to country boundaries. It returns
a DataArray with probability values ranging from 0 - 1. 

The Coordination Reference System of all the raster files used in this function
should be in the same CRS.

Parameters:
    - ncdf: a raster, multi-dimensional global land distribution map with 
    xarray.DataArray type
    - land_type: a str type of the band name representing each land type from 
    the ncdf file
    - date: an int type of the year from the ncdf file (i.e. 1990)
    - reg_name: an object-like or a str type representing the country boundaries
    from a shapefile or raster data format 

Returns:
    - xarray.DataArray type with probability values ranging from 0 - 1
"""

# fun to get the area of each land_type
def clipped_area(reg_diff, area_df, reg_name):
    reg = reg_name
    # g_reg = reg.geometry
    area_clip = area_df.rio.clip_box(minx = min(reg.bounds.minx),
                                     miny = min(reg.bounds.miny),
                                     maxx = max(reg.bounds.maxx),
                                     maxy = max(reg.bounds.maxy))
    # alternative, pbb faster
    #area_clip = area_df.rio.clip_box(*g_reg.bounds, crs=area_df.rio.crs)
    
    # tidying the naming of lon/lat
    area_tidy = area_clip.rename({'y': 'lat', 'x': 'lon'})
    
    # calculate new forests' land  area
    return (reg_diff * area_tidy)

# function to get the cell size from the distribution map

def land_area_clipped(area_df, reg_name, land_dist_map):
    reg = reg_name
    g_reg = reg.geometry
    
    area_clip = area_df.rio.clip(g_reg, crs=area_df.rio.crs)
    
    area_tidy = area_clip.rename({'y': 'lat', 'x': 'lon'})
    
    return (land_dist_map * area_tidy).sum()

# function to plot the cell size from the distribution map

def land_area_plot(area_df, reg_name, land_dist_map):
    reg = reg_name
    g_reg = reg.geometry
    
    area_clip = area_df.rio.clip(g_reg, crs=area_df.rio.crs)
    
    area_tidy = area_clip.rename({'y': 'lat', 'x': 'lon'})
    
    return (land_dist_map * area_tidy)

"""
land_area_clipped calculates the size area of a distribution map for a given
land use type in a given country at one-year time-point. It returns the area
for a given land use type in km2.

The Coordination Reference System of all the raster files used in this function
should be in the same CRS.

Parameters:
    - area_df: a raster in xarray.DataArray containing the information of cell size
    - reg_name: an object-like or a str type representing the country boundaries
    from a shapefile or raster data format 
    - land_dist_map: a raster in xarray.DataArray that contains the possibility 
    values ranging from 0 - 1 of the occurence of a given land use type cropped
    to a given country boundary. 

Returns:
    - A raster file in xarray.DataArray type contains the area of each cell
    given to the probability of the occurrence of a given land use type at a
    country-level. 
"""

# function to calculate land area for a specific land type per year

def land_area_df(start_period, ncdf_input, give_land_type, reg_boundary, area_input): 
    years_for_loop = list(ncdf_input.time.values)
    
    # filtering years
    filter_years = []
    for time in years_for_loop:
        if time >= start_period:
            filter_years.append(time)
    
    # list to keep land area
    
    land_area_ls = []
    
    for yr in filter_years:
        landtype_dist = land_dist(ncdf=ncdf_input, land_type=give_land_type,
                                  date=yr, reg_name=reg_boundary)
        land_area_ls.append((land_area_clipped(area_df=area_input, 
                                               reg_name=reg_boundary, 
                                               land_dist_map=landtype_dist)).values)
    
    land_area_df = pd.DataFrame({"Years" : filter_years, 
                                 "land_area_km2" : land_area_ls})
    land_area_df = land_area_df.astype({'land_area_km2' : 'float32'})
    
    return land_area_df

"""
land_area_df creates a DataFrame containing the temporal evolution of land area
of a given land use type from the earliest year possible to 2100. It gives a 
temporal evolution at country level. 

The Coordination Reference System of all the raster files used in this function
should be in the same CRS.

Parameters:
    - start_period: an int type stating the starting year to begin the calculation
    of land area ofa given land use type
    - ncdf_input: a raster, multi-dimensional global land distribution map with 
    xarray.DataArray type
    - give_land_type: a str type of the band name representing each land type from 
    the ncdf file
    - reg_boundary: an object-like or a str type representing the country boundaries
    from a shapefile or raster data format 
    - area_input: a raster in xarray.DataArray containing the information of cell size
"""

def land_area_changes(start_period, area_input, ncdf_input, give_land_type, reg_boundary, year_prev=None):
    years = list(ncdf_input.time.values)
    # filtering years
    filter_years = []
    for time in years:
        if time >= start_period:
            filter_years.append(time)
            
    land_area = area_input.rio.write_crs(4326)
    
    forest_area_added = []
    forest_area_lost = []
    
    if year_prev==None:
        year_before, year_now = filter_years[:-1], filter_years[1:]
    else:
        year_prev_ls = [year_prev] * len(filter_years)
        year_before, year_now = year_prev_ls, filter_years[1:]
    
    for x_before, x_now in zip(year_before, year_now):
        forest_ratio_prev = land_dist(ncdf=ncdf_input, land_type=give_land_type,
                                      date=x_before, reg_name=reg_boundary)
        forest_ratio_now = land_dist(ncdf=ncdf_input, land_type=give_land_type,
                                     date=x_now, reg_name=reg_boundary)
        forest_ratio_diff = forest_ratio_now - forest_ratio_prev

        forest_ratio_added = forest_ratio_diff.where(forest_ratio_diff > 0)
        forest_area_added.append((land_area_clipped(area_df=land_area, 
                                                    reg_name=reg_boundary, 
                                                    land_dist_map=forest_ratio_added)).values)

        forest_ratio_lost = forest_ratio_diff.where(forest_ratio_diff < 0)
        forest_area_lost.append((land_area_clipped(area_df=land_area, 
                                                   reg_name=reg_boundary, 
                                                   land_dist_map=forest_ratio_lost)).values)
    
    land_area_ch = pd.DataFrame({"Years" : filter_years[1:], 
                                 "forest_area_added" : forest_area_added, 
                                 "forest_area_lost" : forest_area_lost})
                                      
    land_area_ch["net_change"] = land_area_ch["forest_area_added"] + land_area_ch["forest_area_lost"]
    land_area_ch["gross_change"] = land_area_ch["forest_area_added"] - land_area_ch["forest_area_lost"]
    
    return land_area_ch      

"""
"""

def get_ratio(country_forest_ch, regional_forest_ch):
    ems_ratio_ls = country_forest_ch["forest_area_lost"] / regional_forest_ch["forest_area_lost"]
    seq_ratio_ls = country_forest_ch["forest_area_added"] / regional_forest_ch["forest_area_added"]
    
    years_ls = country_forest_ch.Years
    ratio_df = pd.DataFrame({"Years" : years_ls, "ems_ratio" : ems_ratio_ls, 
                             "seq_ratio" : seq_ratio_ls})
    
    return ratio_df       

"""
"""

def downscale_ems_seq(df_input_long, give_vars, give_name, ds_ratio, ratio_vals):
    output_df = df_input_long.loc[df_input_long.Variable == give_vars].reset_index(drop=True)
    
    output_df.loc[:, give_name] = output_df["values"] * ds_ratio[ratio_vals]
    
    return output_df       

"""
"""

def downscale_netemis(country_emis, emis_vals, country_seq, seq_vals, 
                      df_input_long, give_vars, give_name):
    calc = country_emis[emis_vals] + (-(country_seq[seq_vals]))
    output_df = df_input_long.loc[df_input_long.Variable == give_vars].reset_index(drop=True)
    output_df.loc[:, give_name] = calc
    
    return output_df

"""
"""